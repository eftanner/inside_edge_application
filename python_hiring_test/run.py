"""Main script for generating output.csv."""
import pandas as pd
import os


def read_raw(relative_loc):
    """
    take string giving path to a csv-formatted file from this directory
    return pandas dataframe
    input string should start with '/'
    """
    wd = os.path.dirname(__file__)
    return pd.read_csv(wd+relative_loc)


def calc_avg(df):
    """
    Take dataframe, return series.
    Calculates batting average
    """
    return df['H']/df['AB']


def calc_obp(df):
    """
    Take dataframe, return series.
    Calculates on-base perentage
    """
    return (df['H']+df['BB']+df['HBP'])/(df['AB']+df['BB']+df['HBP']+df['SF'])


def calc_slg(df):
    """
    Take dataframe, return series.
    Calculates slugging average
    """
    return df['TB']/df['AB']


def calc_ops(df):
    """
    Take dataframe, return series.
    Calculates on-base plus slugging
    """
    obp = calc_obp(df)
    slg = calc_slg(df)
    return obp+slg


def build_combinations(data, combos, pa_floor=25, round_to=3):
    """
    Take two dataframes, of raw data and requested combinations,
    respectively,
    Return dataframe
    """
    values = {'AVG': calc_avg, 'OBP': calc_obp, 'SLG': calc_slg,
              'OPS': calc_ops}
    keepcols = ['PA', 'AB', 'H', 'TB', 'BB', 'SF', 'HBP']
    dfs = []
    for combo in combos.to_records(index=False):
        stat, subject, split = combo
        if split[-2:] == 'HP':
            filtered = data[data['PitcherSide'] == split[-3:-2]]
        else:
            filtered = data[data['HitterSide'] == split[-3:-2]]
        grouped = filtered.groupby(subject)[keepcols].sum()
        out = grouped[grouped['PA'] >= pa_floor].copy()
        out['Value'] = values[stat](out).round(round_to)
        out['Stat'] = stat
        out['Split'] = split
        out['Subject'] = subject
        out['SubjectId'] = out.index
        dfs.append(out.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject',
                               'Value']])
    return pd.concat(dfs).sort_values(['SubjectId', 'Stat', 'Split'])


def main():
    """
    perform all IO for and call build_combinations
    """
    data = read_raw('/data/raw/pitchdata.csv')
    combinations = read_raw('/data/reference/combinations.txt')
    final = build_combinations(data, combinations)
    final.to_csv(os.path.dirname(__file__)+'/data/processed/output.csv',
                 index=False)


if __name__ == '__main__':
    main()
